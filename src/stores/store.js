import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    message: 'Hello from Vuex',
    count: 2
  },
  mutations: { // syncronous
    increment(state, payload) {
      state.count += payload
    }
  }, 
  actions: { // asyncronous
    increment(state, payload) {
      state.commit('increment', payload)
    }
  },
  getters: {
    message(state) {
      return state.message
    },
    count(state) {
      return state.count
    }
  }
})

export default store