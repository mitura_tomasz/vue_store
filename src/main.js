import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import store from './stores/store.js'
import FootballPlayer from './components/FootballPlayer.vue'
import BasketBallPlayer from './components/BasketballPlayer.vue'

Vue.use(Vuex)
Vue.component('app-football-player', FootballPlayer)
Vue.component('app-basketball-player', BasketBallPlayer)


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
